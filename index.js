// console.log("Hey!")


// O B J E C T
/*
	-an object is a data type that is used to present real world objects.

	- represented in a "Key: Value" pair

	- key may be referred to as "property" of an object.

	- an object may be assigned to a different data type such as another object(nested object), an array, boolean, etc.

	Syntax:
		let objectName = {
			keyA: ValueA,
			keyB: ValueB
		}
*/

	let cellphone = {
		name: "Nokia 3210",
		mfgDate: 1999
	}

	console.log("Result from creating using literal notation: ");
	console.log(cellphone);


// Creating objects using constructor function
/*
	- create a reusable function to create several objects that have the same data structure.

	- This is useful for creating multiple instances/copies of an object.

	- An instance is a concrete occurenece of any objects which emphasize unique identity of it.

	Syntax:
		function objectName(valueA, valueB){
			this.keyA = valueA,
			this.keyB = valueB
		}

	- the "this." keyword allows us to assign a new object's properties by associating them with values received from a constructor function's paramater

*/

	function Laptop(name, mfgDate, ram){
		this.laptopName = name;
		this.laptopMfgDate = mfgDate;
		this.laptopRam = ram;
	}

//  Instantiation - the "new" operator create an instances of an object; object and instances are often interchange.

	let laptop = new Laptop('Lenovo', 2008, "2 gb");
	console.log("Result from creating objects using constructor");
	console.log(laptop);

	let myLaptop = new Laptop("MacBook Air", 2020, "8 gb");
	console.log("Result from creating objects using constructor");
	console.log(myLaptop);

// The example below invoke/calls a laptop function instead of creating a new object. It will return "undefined" without the "new" operator because the "laptop" function does not have any return statement 

	let oldLaptop = Laptop("Portal R2E CCMC", 1980, "500 mb")
	console.log("Result from creating objects without the new keyboard");
	console.log(oldLaptop);


// mini-activity: create a constructor function that will let us instantiate a new object, menu - property: menuName, menuPrice.

	function Menu(mealName, mealPrice){
		this.menuName = mealName;
		this.menuPrice = mealPrice;
	}

	let mealOne = new Menu('Breakfast','P 299.00');
	console.log("Result from creating objects using constructor");
	console.log(mealOne);


// Creating empty objects

	let computer = {};
	let myComputer = new Object();
	console.log(computer);
	console.log(myComputer);

// Accessing Objects inside an array

	let array = [laptop, myLaptop]
	console.log(array);
	console.log(array[0])

	console.log(array[0].laptopMfgDate); //dot notation

	console.log(laptop);
	console.log(laptop.laptopRam);

// Initializing/adding/deleting/reassigning object properties.
/*
	- like any other variable in js, objects have their properties initialized/added after the object was declared.
*/

			let car = {};
			console.log(car);

		//adding object property using dot notation
			car.name = 'Honda Civic'; 
			console.log(car);

		// adding object property using bracket notation
			car['mfgDate']=2019; 
			console.log(car);

		// reassigning using dot notation
			car.name = "Dodge Charger R/T" 
			console.log(car)

			car["name"] = "Jeepney" // reassigning using bracket notation
			console.log(car)

			delete car["name"]; //deleting object using bracket notation
			console.log(car);

			delete car.mfgDate; //deleting object using dot notation
			console.log(car);

// Object Methods
/*
	- a method is function which is a property of an object.

	- they are also a functions and one of the key differences they have is that method are functions related to a specific object
*/

	let person = {
		name: 'John',
		talk: function(){
			console.log("Hello my name is " + this.name)
		}
	}
	console.log(person);

	person.talk();

	// adding method to objects using dot notation

		person.walk = function(){
			console.log(this.name + " walked 25 steps")
		}

// methods are useful for creating reusable functions that perform tasks related to objects.

	let friends = {
		firstName: 'Joe',
		lastName: 'Smith',
		address: {
			city: 'Austin',
			country: 'Texas'
		},
		phoneNumber: [['09123456789'],['043-456-789']],
		emails: ['joe@mail.com', 'joesmith@email.xyz'],
		introduce: function(){
			console.log("Hello my Name is: " + this.firstName + " " + this.lastName + ". I leave in " + this.address.city + ", " + this.address.country + ". My emails are " + this.emails[0] + " and " + this.emails[1] + ". My phone numbers are " + this.phoneNumber[0][0] + " and " + this.phoneNumber[1][0])
		}
	}

friends.introduce()

// Create an object constructor

	function Pokemon(name,level){
		// Properties Pokemon
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2*level;
		this.pokemonAttack = level;

		// methods

		this.tackle = function(targetPokemon){
			console.log(this.pokemonName + " takles " + targetPokemon.pokemonName);
			console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
		}
		this.fainted = function(){
			console.log(this.pokemonName + " fainted")
		}

	}

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);
	let gyarados = new Pokemon("Gyarados", 20);
	console.log(gyarados);
	pikachu.tackle(gyarados)
	gyarados.fainted()